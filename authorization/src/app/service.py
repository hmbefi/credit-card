"""Модуль является точкой входа в приложение."""

from contextlib import asynccontextmanager

import uvicorn
from aiohttp import ClientSession
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from prometheus_client import make_asgi_app
from src.app.config.config import settings
from src.app.credit_cards import controller
from src.app.data_sources.adaptor import DatabaseConnection
from src.app.data_sources.storages.user_storage import UserStorage
from src.app.middlewares import metrics_middleware
from src.app.user import controller as user_controller
from starlette.middleware.base import BaseHTTPMiddleware


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Метод производит start и shutdown ивенты.

    Args:
        app (FastAPI): приложение

    Yields:
        None
    """
    connection = DatabaseConnection()
    user_controller.user_storage = UserStorage(session=connection.get_session())
    app.state.session = ClientSession()
    app.state.kafka_producer = AIOKafkaProducer(
        bootstrap_servers='{0}:{1}'.format(settings.kafka.host, settings.kafka.port),
    )
    app.state.kafka_consumer = AIOKafkaConsumer(
        settings.kafka.consumer_topic,
        bootstrap_servers='{0}:{1}'.format(settings.kafka.host, settings.kafka.port),
    )
    await app.state.kafka_producer.start()
    await app.state.kafka_consumer.start()
    yield
    await app.state.kafka_consumer.stop()
    await app.state.kafka_producer.stop()
    await app.state.session.close()


app = FastAPI(lifespan=lifespan)
app.include_router(user_controller.router)
app.include_router(controller.router)

metrics_app = make_asgi_app()
app.mount('/api/metrics', metrics_app)
app.add_middleware(BaseHTTPMiddleware, dispatch=metrics_middleware)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=settings.service.host,
        port=settings.service.port,
        timeout_graceful_shutdown=1,
    )
