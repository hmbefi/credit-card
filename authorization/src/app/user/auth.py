"""Модуль содержащий функции для аутентификации пользователя."""

from bcrypt import checkpw
from src.app.api.schemas import User
from src.app.data_sources.storages.user_storage import UserStorage


async def authenticate_user(storage: UserStorage, username: str, password: str) -> User:
    """Метод для проверки пароля пользователя.

    Args:
        storage (UserStorage): хранилище пользователей
        username (str): имя пользователя
        password (str): пароль пользователя

    Returns:
        User: пользователь
    """
    user = await storage.get_user(username=username)
    if not user:
        return False
    if not checkpw(password.encode(), user.password_hash.encode()):
        return False
    return User(username=username)
