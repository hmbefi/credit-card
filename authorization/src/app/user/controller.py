"""Модуль содержащий эндпоинты связанные с пользователем."""

import fastapi
from aiohttp import ClientConnectionError
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from prometheus_client import Enum
from src.app.api.schemas import AccessToken
from src.app.config.config import settings
from src.app.user.access_token import create_access_token
from src.app.user.auth import authenticate_user
from typing_extensions import Annotated

router = fastapi.APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/api/auth')
user_storage = None


async def check_access_token(access_token: Annotated[str, fastapi.Depends(oauth2_scheme)]):
    """Метод для проверки токена доступа.

    Args:
        access_token (Annotated[str, Depends): jwt токен

    Raises:
        HTTPException: не удалось декодировать токен
    """
    try:
        jwt.decode(
            access_token,
            settings.access_token.secret,
            algorithms='HS256',
        )
    except JWTError:
        raise fastapi.HTTPException(status_code=fastapi.status.HTTP_401_UNAUTHORIZED)


ready_status = Enum(
    'hmbefi_auth_service_ready_status',
    'Service readiness',
    states=['not_ready', 'ready'],
)


@router.get('/api/ready')
async def ready(request: fastapi.Request):
    """Ready проба.

    Args:
        request (Request): полученный реквест

    Returns:
        Response: статус сервиса
    """
    try:
        resp = await request.app.state.session.get(
            url='http://{0}:{1}/api/ready'.format(
                settings.main_service.host,
                settings.main_service.port,
            ),
        )
    except ClientConnectionError:
        ready_status.state('not_ready')
        return fastapi.responses.Response(
            status_code=fastapi.status.HTTP_503_SERVICE_UNAVAILABLE,
        )
    if resp.status == fastapi.status.HTTP_200_OK:
        ready_status.state('ready')
        return fastapi.responses.Response(
            status_code=fastapi.status.HTTP_200_OK,
        )
    ready_status.state('not_ready')
    return fastapi.responses.Response(
        status_code=fastapi.status.HTTP_503_SERVICE_UNAVAILABLE,
    )


@router.get('/api/live')
async def live():
    """Liveness проба.

    Returns:
        Response: статус сервиса
    """
    return fastapi.responses.Response(
        status_code=fastapi.status.HTTP_200_OK,
    )


@router.post('/api/register')
async def register(
    username: Annotated[str, fastapi.Query(max_length=100)],
    password: Annotated[str, fastapi.Query(max_length=100)],
):
    """Метод добавления новых пользователей.

    Args:
        username (str): имя пользователя
        password (str): пароль

    Raises:
        HTTPException: пользователь с таким именем уже существует
    """
    try:
        await user_storage.add(username=username, password=password)
    except ValueError as exception:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )


@router.post('/api/auth')
async def auth_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, fastapi.Depends()],
) -> AccessToken:
    """Аутентификация пользователя для получения jwt.

    Args:
        form_data (Annotated[OAuth2PasswordRequestForm, Depends):
        OAuth2 форма аутентификации

    Raises:
        HTTPException: неверрные данные пользователя

    Returns:
        AccessToken: jwt и тип токена
    """
    user = await authenticate_user(
        storage=user_storage,
        username=form_data.username,
        password=form_data.password,
    )
    if not user:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    access_token = create_access_token(user.username)
    return AccessToken(access_token=access_token, token_type=settings.access_token.token_type)
