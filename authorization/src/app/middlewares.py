"""Модуль содержит методы реализующие middleware."""
import time

from fastapi import Request, Response, status
from prometheus_client import Histogram

request_latency_histogram = Histogram(
    'hmbefi_auth_service_request_latency_histogram',  # Название метрики
    'Request latency.',  # Документация метрики
    ['operation', 'http_status_code', 'error'],  # Лейблы
)


async def metrics_middleware(request: Request, call_next):
    """Middleware для реализации логгирования времени выполнения запроса.

    Args:
        request (Request): измеряемый запрос
        call_next (Callable): функция которая передаст реквест хэндлеру и вернет ответ

    Returns:
        Response: ответ
    """
    start_time = time.monotonic()
    response: Response = await call_next(request)
    operation = '{0} {1}'.format(
        request.method,
        request.url.path,
    )
    request_latency_histogram.labels(
        operation,
        response.status_code,
        response.status_code >= status.HTTP_400_BAD_REQUEST,
    ).observe(
        time.monotonic() - start_time,
    )
    return response
