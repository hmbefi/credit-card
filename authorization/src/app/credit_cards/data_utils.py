"""Модуль содержит вспомогательные методы."""

import json

import aiofiles
from aiokafka.structs import ConsumerRecord
from fastapi import HTTPException, UploadFile, status
from pydantic import ValidationError
from src.app.config.config import settings

URL_BASE = 'http://{0}:{1}'.format(settings.main_service.host, settings.main_service.port)


def url(url_end: str) -> str:
    """Конструктор url.

    Args:
        url_end (str): окончание url

    Returns:
        str: url
    """
    return URL_BASE + url_end


def validate_data(response_data: dict | list, model):
    """Валидация данных.

    Args:
        response_data (dict | list): данные
        model (Pydantic Model): pydantic модель для валидации

    Returns:
        dict | list: провалидированные данные
    """
    if isinstance(response_data, list):
        for count, part in enumerate(response_data):
            response_data[count] = model(**part)
    else:
        response_data = model(**response_data)
    return response_data


def validate_response(
    response_data: dict | list | ConsumerRecord,
    response_status: int,
    model=None,
):
    """Валидация ответа сервиса.

    Args:
        response_data (dict | list | ConsumerRecord): ответ сервиса
        response_status (int): статус ответа
        model (Pydantic Model): pydantic модель для валидации

    Raises:
        HTTPException: ошибка сервиса
        HTTPException: ошибка валидации данных

    Returns:
        dict | list: провалидированные данные
    """
    if isinstance(response_data, ConsumerRecord):
        response_data = json.loads(response_data.value.decode('utf-8').replace("'", '"'))
    if response_status != status.HTTP_200_OK:
        raise HTTPException(response_status, detail=response_data.get('detail'))
    if model:
        try:
            response_data = validate_data(response_data, model)
        except ValidationError:
            raise HTTPException(
                status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=str('Internal validation error'),
            )
    return response_data


async def save_photo(photo: UploadFile, filename: str):
    """Сохранение фотографий.

    Args:
        photo (UploadFile): фото
        filename (str): имя файла для сохранения
    """
    async with aiofiles.open('./data/{0}'.format(filename), 'wb') as out_file:
        photo_data = await photo.read()
        await out_file.write(photo_data)
