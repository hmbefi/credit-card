"""Модуль содержащий эндпоинты связанные с кредитными картами."""

from datetime import datetime
from decimal import Decimal
from typing import Annotated

import fastapi
from prometheus_client import Counter
from src.app.api import schemas
from src.app.config.config import settings
from src.app.credit_cards import data_utils
from src.app.user.controller import check_access_token

router = fastapi.APIRouter(dependencies=[fastapi.Depends(check_access_token)])


@router.post('/api/add')
async def add_card(
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
):
    """Добавление новой карты.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты
    """
    session = request.app.state.session
    request_params = {'card_number': card_number}
    async with session.post(
        url=data_utils.url('/api/add'),
        params=request_params,
    ) as resp:
        response_data = await resp.json()
        data_utils.validate_response(response_data, resp.status)


@router.get('/api/balance')
async def get_balance(
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
) -> schemas.BalanceResponse:
    """Получение баланса карты.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты

    Returns:
        BalanceResponse: баланс карты
    """
    session = request.app.state.session
    request_params = {'card_number': card_number}
    async with session.get(
        url=data_utils.url('/api/balance'),
        params=request_params,
    ) as resp:
        response_data = await resp.json()
        return data_utils.validate_response(
            response_data,
            resp.status,
            schemas.BalanceResponse,
        )


@router.get('/api/balance/history')
async def get_balance_history(
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
    from_datetime: datetime,
    to_datetime: datetime,
) -> list[schemas.BalanceLogResponse]:
    """Получение истории изменений баланса по карте.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты
        from_datetime (datetime):
        дата и время с которых начинается поиск записей
        to_datetime (datetime):
        дата и время на которых заканчивается поиск записей

    Raises:
        HTTPException: from_datetime должно быть раньше to_datetime

    Returns:
        list[dict]: список записей за указанный промежуток времени
    """
    if from_datetime >= to_datetime:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_400_BAD_REQUEST,
            detail='Указаны неверные границы промежутка времени',
        )
    session = request.app.state.session
    request_params = {
        'card_number': card_number,
        'from_datetime': str(from_datetime),
        'to_datetime': str(to_datetime),
    }
    async with session.get(
        url=data_utils.url('/api/balance/history'),
        params=request_params,
    ) as resp:
        response_data = await resp.json()
        return data_utils.validate_response(
            response_data,
            resp.status,
            schemas.BalanceLogResponse,
        )


@router.get('/api/withdrawal')
async def withdrawal(
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
    amount: Annotated[Decimal, fastapi.Query(max_digits=50, decimal_places=2)],
):
    """Метод вывода средств с карты.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты
        amount (Decimal): значение для вывода
    """
    session = request.app.state.session
    request_params = {
        'card_number': card_number,
        'amount': str(amount),
    }
    async with session.get(
        url=data_utils.url('/api/withdrawal'),
        params=request_params,
    ) as resp:
        response_data = await resp.json()
        data_utils.validate_response(response_data, resp.status)


@router.get('/api/deposit')
async def deposit(
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
    amount: Annotated[Decimal, fastapi.Query(max_digits=50, decimal_places=2)],
):
    """Метод перевода средств на карту.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты
        amount (Decimal): значение для перевода
    """
    session = request.app.state.session
    request_params = {
        'card_number': card_number,
        'amount': str(amount),
    }
    async with session.get(
        url=data_utils.url('/api/deposit'),
        params=request_params,
    ) as resp:
        response_data = await resp.json()
        data_utils.validate_response(response_data, resp.status)


verification_result_counter = Counter(
    'hmbefi_auth_service_verification_result_counter',
    'Counter of succeed and failed verification attempts',
    ['result'],
)


@router.post('/api/verify')
async def verify(  # noqa: WPS210
    request: fastapi.Request,
    card_number: Annotated[str, fastapi.Query(max_length=16)],
    photo: fastapi.UploadFile,
    document_photo: fastapi.UploadFile,
):
    """Метод для верификации и изменения кредитного лимита.

    Args:
        request (Request): полученный реквест
        card_number (str): номер карты
        photo (fastapi.UploadFile): фото лица
        document_photo (fastapi.UploadFile): фото документа

    Returns:
        VerificationResponse: результат верификации
    """
    async with request.app.state.session.get(
        url=data_utils.url('/api/balance'),
        params={'card_number': card_number},
    ) as resp:
        response_data = await resp.json()
        data_utils.validate_response(response_data, resp.status, schemas.BalanceResponse)
    timestamp = str(int(datetime.utcnow().timestamp() * 1000))
    photo_name = '{0}_{1}'.format(card_number, timestamp)
    doc_photo_name = 'docs_{0}'.format(photo_name)
    await data_utils.save_photo(photo=photo, filename=photo_name)
    await data_utils.save_photo(photo=document_photo, filename=doc_photo_name)
    dict_data = {
        'card_number': card_number,
        'photo_name': photo_name,
        'doc_photo_name': doc_photo_name,
    }
    await request.app.state.kafka_producer.send(
        topic=settings.kafka.producer_topic,
        value=bytes(str(dict_data), encoding='utf-8'),
    )
    for _ in range(settings.kafka.consumer_retries):
        verification_response = await request.app.state.kafka_consumer.getone()
        verification_response = data_utils.validate_response(
            response_data=verification_response,
            response_status=fastapi.status.HTTP_200_OK,
            model=schemas.VerificationResponse,
        )
        if verification_response.card_number == card_number:
            if verification_response.verified:
                verification_result_counter.labels('verified').inc()
            else:
                verification_result_counter.labels('not_verified').inc()
            return verification_response
