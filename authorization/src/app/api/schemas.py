"""Модуль содержит модели (схемы) для валидации данных."""

from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel, Field


class User(BaseModel):
    """Класс пользователя."""

    username: str = Field(max_length=100)


class AccessToken(BaseModel):
    """Класс временного токена доступа."""

    access_token: str
    token_type: str


class Transaction(BaseModel):
    """Класс валидации транзакции списания или начисления."""

    card_number: str = Field(max_length=16)
    amount: Decimal = Field(max_digits=50, decimal_places=2)


class BalanceResponse(BaseModel):
    """Класс ответа на запрос баланса."""

    card_number: str = Field(max_length=16)
    balance: Decimal = Field(max_digits=50, decimal_places=2)


class BalanceLogResponse(BaseModel):
    """Класс ответа на запрос истории баланса."""

    card_number: str = Field(max_length=16)
    before: Decimal = Field(max_digits=50, decimal_places=2)
    after: Decimal = Field(max_digits=50, decimal_places=2)
    changes: Decimal = Field(max_digits=50, decimal_places=2)
    datetime_utc: datetime


class LimitResponse(BaseModel):
    """Класс ответа на запрос лимита."""

    card_number: str = Field(max_length=16)
    limit: Decimal = Field(max_digits=50, decimal_places=2)


class VerificationResponse(BaseModel):
    """Класс отета на запрос верификации."""

    card_number: str = Field(max_length=16)
    verified: bool
