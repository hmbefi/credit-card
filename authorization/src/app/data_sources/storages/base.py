"""Модуль содержит базовый класс хранилища."""
from sqlalchemy.ext.asyncio import AsyncSession


class BaseStorage(object):
    """Базовый класс хранилища."""

    def __init__(self, session: AsyncSession):
        self._session = session
