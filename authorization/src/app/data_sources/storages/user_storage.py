"""Модуль содержит класс UserStorage."""

from bcrypt import gensalt, hashpw
from sqlalchemy import insert, select
from src.app.data_sources.dtos.user import User
from src.app.data_sources.models import UserAlchemyModel
from src.app.data_sources.storages.base import BaseStorage


class UserStorage(BaseStorage):
    """Класс хранилища пользователей."""

    async def add(self, username: str, password: str):
        """Метод добавления новых пользователей.

        Args:
            username (str): имя пользователя
            password (str): пароль

        Raises:
            ValueError: пользователь с таким именем уже существует
        """
        user = (await self._session.execute(
            select(UserAlchemyModel).where(
                UserAlchemyModel.username == username,
            ),
        )).scalar()

        if user:
            raise ValueError('Пользователь с таким именем уже существует')

        await self._session.execute(
            insert(UserAlchemyModel).values(
                username=username,
                password_hash=hashpw(password.encode(), gensalt()).decode(),
            ),
        )
        await self._session.commit()

    async def get_user(self, username: str):
        """Метод получения пользователя по username.

        Args:
            username (str): имя пользователя

        Returns:
            User: пользователь
        """
        user = (await self._session.execute(
            select(UserAlchemyModel).where(
                UserAlchemyModel.username == username,
            ),
        )).scalar()
        if user:
            return User.from_alchemy(user)
