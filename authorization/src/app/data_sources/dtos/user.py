"""Модуль содержит датакласс User."""
from dataclasses import dataclass

from src.app.data_sources.models.user import UserAlchemyModel


@dataclass
class User(object):
    """Класс пользователя."""

    username: str
    password_hash: str

    @classmethod
    def from_alchemy(cls, user: UserAlchemyModel):
        """Метод инициализации пользователя на данных из бд.

        Args:
            user (UserAlchemyModel): запись пользователя из бд

        Returns:
            User: пользователь
        """
        return cls(
            username=user.username,
            password_hash=user.password_hash,
        )
