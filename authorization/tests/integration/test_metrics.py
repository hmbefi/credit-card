import re

from aiohttp import ClientSession
from fastapi import status
import pytest

from src.app.config.config import settings


@pytest.mark.asyncio
async def test_get_metrics(jwt_fixture):
    async with ClientSession() as session:
        headers = {'Authorization': f'Bearer {jwt_fixture}'}

        resp = await session.get(
            'http://{0}:{1}/api/ready'.format(
                settings.service.host,
                settings.service.port,
            ),
        )
        assert resp.status == status.HTTP_200_OK

        resp = await session.post(
            'http://{0}:{1}/api/add'.format(
                settings.service.host,
                settings.service.port,
            ),
            params={'card_number': 'test'},
            headers=headers,
        )
        if resp.status != status.HTTP_200_OK:
            resp = await resp.json()
            error_detail = resp.get('detail')
            assert error_detail == 'Карта с таким номером уже существует'

        photo = open('tests/integration/credit_cards/photos/selfie.jpg', 'rb')
        document_photo = open('tests/integration/credit_cards/photos/document_photo.png', 'rb')
        try:
            photos = {'photo': photo.read(), 'document_photo': document_photo.read()}
        finally:
            photo.close()
            document_photo.close()

        resp = await session.post(
            'http://{0}:{1}/api/verify'.format(
                settings.service.host,
                settings.service.port,
            ),
            params={'card_number': 'test'},
            data=photos,
            headers=headers,
        )
        assert resp.status == status.HTTP_200_OK

        resp = await session.get(
            'http://{0}:{1}/api/metrics'.format(
                settings.service.host,
                settings.service.port,
            ),
        )
        resp = await resp.text()
        resp_lines = resp.splitlines()
        assert 'auth_service_ready_status{auth_service_ready_status="ready"} 1.0' in resp_lines  # ready metric
        reg = re.compile(r'auth_service_verification_result_counter_total{result="verified"} \d+.0')  # limit change counter metric
        assert any([reg.search(line) for line in resp_lines])
        #  latency and endpoint/status_code metric
        reg = re.compile(r'auth_service_request_latency_histogram_bucket{error="False",http_status_code="200",le="\d+\.\d+",operation="POST /api/verify"} \d+.0')
        assert any([reg.search(line) for line in resp_lines])
