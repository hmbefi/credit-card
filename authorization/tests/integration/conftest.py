from datetime import datetime, timedelta

import pytest_asyncio
from jose import jwt

from src.app.config.config import settings


@pytest_asyncio.fixture
def jwt_fixture():
    data = {
        'sub': 'test',
        'exp': datetime.utcnow() + timedelta(days=7),
    }
    return jwt.encode(
        data,
        settings.access_token.secret,
        algorithm='HS256',
    )
