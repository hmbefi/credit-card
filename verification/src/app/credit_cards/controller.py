"""Модуль содержащий эндпоинты связанные с кредитными картами."""

import asyncio
import json
from decimal import Decimal

import aiofiles
from aiohttp import ClientConnectionError, ClientSession
from aiokafka.structs import ConsumerRecord
from fastapi import APIRouter, HTTPException, Request, responses, status
from pydantic import ValidationError
from src.app.api.schemas import VerificationMessage
from src.app.config.config import settings
from src.app.credit_cards.verification import verify_photo

router = APIRouter()


@router.get('/api/ready')
async def ready(request: Request):
    """Ready проба.

    Args:
        request (Request): полученный реквест

    Returns:
        Response: статус сервиса
    """
    try:
        resp = await request.app.state.session.get(
            url='http://{0}:{1}/api/ready'.format(
                settings.main_service.host,
                settings.main_service.port,
            ),
        )
    except ClientConnectionError:
        return responses.Response(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)
    if resp.status == status.HTTP_200_OK:
        return responses.Response(status_code=status.HTTP_200_OK)
    return responses.Response(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)


@router.get('/api/live')
async def live():
    """Liveness проба.

    Returns:
        Response: статус сервиса
    """
    return responses.Response(status_code=status.HTTP_200_OK)


async def read_photo(filename: str) -> bytes:
    """Чтение фотографий.

    Args:
        filename (str): имя файла

    Returns:
        bytes: фото в формате байт
    """
    async with aiofiles.open(file='./data/{0}'.format(filename), mode='rb') as photo:
        return await photo.read()


async def process_verification_message(message: ConsumerRecord) -> dict:
    """Обработка сообщения верификации.

    Args:
        message (ConsumerRecord): сообщение

    Returns:
        dict: обработанное сообщение
    """
    new_verification = json.loads(message.value.decode('utf-8').replace("'", '"'))
    try:
        new_verification = VerificationMessage(**new_verification)
    except ValidationError as exception:
        print(exception)  # noqa: WPS421
        return None
    photo = await read_photo(new_verification.photo_name)
    doc_photo = await read_photo(new_verification.doc_photo_name)
    return {
        'card_number': new_verification.card_number,
        'photo': photo,
        'doc_photo': doc_photo,
    }


async def request_limit_change(session: ClientSession, card_number: str, limit: Decimal):
    """Запрос на изменение лимита.

    Args:
        session (ClientSession): сессия aiohttp
        card_number (str): номер карты
        limit (Decimal): новый лимит

    Raises:
        HTTPException: не удалось увеличить лимит
    """
    request_params = {
        'card_number': card_number,
        'limit': str(limit),
    }
    async with session.get(
        url='http://{0}:{1}/api/limit/change'.format(
            settings.main_service.host,
            settings.main_service.port,
        ),
        params=request_params,
    ) as resp:
        if resp.status != status.HTTP_200_OK:
            raise HTTPException(resp.status, detail='Не удалось увеличить лимит')


async def verify_users(resources: dict):  # noqa: WPS217
    """Обработка сообщений из kafka.

    Args:
        resources (dict): ресурсы созданные при старте fastapi
    """
    while True:  # noqa: WPS457
        await asyncio.sleep(1)
        new_message = await resources['kafka_consumer'].getone()
        new_verification = await process_verification_message(message=new_message)
        if not new_verification:
            continue
        verified = await verify_photo(
            process_pool_executor=resources['process_pool_executor'],
            photo=new_verification['photo'],
            document_photo=new_verification['doc_photo'],
        )
        limit = settings.card.not_verified_limit
        if verified:
            limit = settings.card.verified_limit
        await request_limit_change(
            session=resources['session'],
            card_number=new_verification['card_number'],
            limit=limit,
        )
        verified = str(verified).lower()
        await resources['kafka_producer'].send(
            topic=settings.kafka.producer_topic,
            value=bytes(
                str({'card_number': new_verification['card_number'], 'verified': verified}),
                encoding='utf-8',
            ),
        )
