"""Модуль содержит метод для верификации обладателя карты."""

import asyncio
from functools import partial

import cv2
import numpy as np
from deepface import DeepFace


def verify(photo=bytes, document_photo=bytes) -> dict:
    """Метод конвертирует bytes в формат изображения и вызывает verify у модели.

    Args:
        photo (bytes): фото лица
        document_photo (bytes): фото документа

    Returns:
        dict: ответ модели
    """
    photo = np.frombuffer(photo, np.uint8)
    photo = cv2.imdecode(photo, cv2.IMREAD_COLOR)
    document_photo = np.frombuffer(document_photo, np.uint8)
    document_photo = cv2.imdecode(document_photo, cv2.IMREAD_COLOR)
    return DeepFace.verify(photo, document_photo)


async def verify_photo(process_pool_executor, photo=bytes, document_photo=bytes) -> bool:
    """Метод для верификации фотографий с помощью deepface.

    Args:
        process_pool_executor (ProcessPoolExecutor): пул процессов
        photo (bytes): фото лица
        document_photo (bytes): фото документа

    Returns:
        bool: результат верификации
    """
    loop = asyncio.get_event_loop()
    model_response = await loop.run_in_executor(
        process_pool_executor,
        partial(
            verify,
            photo,
            document_photo,
        ),
    )
    return bool(model_response.get('verified'))
