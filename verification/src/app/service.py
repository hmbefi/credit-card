"""Модуль является точкой входа в приложение."""

import pathlib
from concurrent.futures import ProcessPoolExecutor
from contextlib import asynccontextmanager

import aiojobs
import gdown
import uvicorn
from aiohttp import ClientSession
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from fastapi import FastAPI
from src.app.config.config import settings
from src.app.credit_cards import controller

resources = {}


def check_model():
    """Метод проверяет наличие файлов для работы модели и при их отсутствии загружает."""
    path = str(pathlib.Path.home()) + settings.model.weights_path
    if not pathlib.Path(path).is_file():
        gdown.download(settings.model.weights_url, path, quiet=False)


async def start_kafka():
    """Запуск клиентов кафки."""
    await resources['kafka_consumer'].start()
    await resources['kafka_producer'].start()


async def stop_kafka():
    """Остановка клиентов кафки."""
    await resources['kafka_producer'].stop()
    await resources['kafka_consumer'].stop()


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Метод производит start и shutdown ивенты.

    Args:
        app (FastAPI): приложение

    Yields:
        None
    """
    check_model()
    scheduler = aiojobs.Scheduler()
    resources['session'] = ClientSession()
    app.state.session = resources['session']
    resources['kafka_consumer'] = AIOKafkaConsumer(
        settings.kafka.consumer_topic,
        bootstrap_servers='{0}:{1}'.format(settings.kafka.host, settings.kafka.port),
    )
    resources['kafka_producer'] = AIOKafkaProducer(
        bootstrap_servers='{0}:{1}'.format(settings.kafka.host, settings.kafka.port),
    )
    await start_kafka()
    resources['process_pool_executor'] = ProcessPoolExecutor(max_workers=1)
    await scheduler.spawn(controller.verify_users(resources=resources))
    yield
    await scheduler.close()
    resources['process_pool_executor'].shutdown()
    await stop_kafka()
    await resources['session'].close()


app = FastAPI(lifespan=lifespan)
app.include_router(controller.router)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=settings.service.host,
        port=settings.service.port,
        timeout_graceful_shutdown=1,
    )
