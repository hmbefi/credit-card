"""
Модуль для конфигурации проекта.

Позволяет импортировать конфигурацию из config.yml файла.
"""

from decimal import Decimal
from pathlib import Path

import yaml
from pydantic_settings import BaseSettings, SettingsConfigDict


class _SettingsModel(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='EMP_', env_nested_delimiter='_')

    @classmethod
    def from_yaml(cls, config_path: str) -> '_SettingsModel':
        return cls(**yaml.safe_load(Path(config_path).read_text()))

    @classmethod
    def settings_customise_sources(  # noqa: WPS211
        cls,
        settings_cls,
        init_settings,
        env_settings,
        dotenv_settings,
        file_secret_settings,
    ):
        return env_settings, init_settings, file_secret_settings


class _ServiceSettings(_SettingsModel):
    host: str
    port: int


class _MainServiceSettings(_SettingsModel):
    host: str
    port: int


class _KafkaSettings(_SettingsModel):
    host: str
    port: int
    producer_topic: str
    consumer_topic: str


class _CardSettings(_SettingsModel):
    not_verified_limit: Decimal
    verified_limit: Decimal


class _ModelSettings(_SettingsModel):
    weights_path: str
    weights_url: str


class Settings(_SettingsModel):
    """Настройки сервиса."""

    service: _ServiceSettings
    main_service: _MainServiceSettings
    kafka: _KafkaSettings
    card: _CardSettings
    model: _ModelSettings


settings = Settings.from_yaml('src/app/config/config.yml')
