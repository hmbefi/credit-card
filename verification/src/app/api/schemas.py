"""Модуль содержит модели (схемы) для валидации данных."""

from pydantic import BaseModel


class VerificationMessage(BaseModel):
    """Сообщение верификации от kafka."""

    card_number: str
    photo_name: str
    doc_photo_name: str
