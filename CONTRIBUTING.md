### Миграции
Накат миграций
```sh
docker exec -it credit-card-main-1 alembic upgrade head
```

Для локального тестирования миграций:
```sh
docker run -d -e POSTGRES_PASSWORD='admin' -e POSTGRES_USER='admin' -p 5432:5432 --name=postgres-test postgres
cd main | poetry run pytest tests/migrations
```