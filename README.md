### Создание сертификата
Создание сертификата:
```sh
cd authorization | ./create_cert.sh
```

### Docker compose
Поднятие сервисов:
```sh
docker-compose up -d
```

Остановка сервисов:
```sh
docker-compose down
```

### Tests
Для запуска тестов сервиса main:
```sh
docker run -d -e POSTGRES_PASSWORD='admin' -e POSTGRES_USER='admin' -p 5432:5432 --name=postgres-test postgres
poetry run pytest tests
```
