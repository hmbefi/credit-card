import asyncio

import asyncpg
import pytest
import pytest_asyncio
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from src.app.config.config import settings
from src.app.data_sources.models import Base


@pytest.fixture(scope='session')
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope='session')
async def config():
    settings.postgres.db_name = f'{settings.postgres.db_name}_test'
    yield settings


@pytest_asyncio.fixture(scope='session')
async def flush_db(config):
    if not settings.postgres.db_name.endswith('_test'):
        raise ValueError('Для запуска тестов settings.postgres.db_name в конфиге должно быть вида *_test')

    postgres_table_uri = f'postgresql://{settings.postgres.url}/postgres'

    connection = await asyncpg.connect(postgres_table_uri)
    await connection.execute(f'DROP DATABASE IF EXISTS {settings.postgres.db_name}')
    await connection.execute(f'CREATE DATABASE {settings.postgres.db_name}')
    await connection.close()


@pytest_asyncio.fixture(scope='session')
async def db_engine(config):
    yield create_async_engine(settings.postgres.uri, echo=False)


@pytest_asyncio.fixture(scope='session')
async def new_db_schema(flush_db, db_engine):
    async with db_engine.begin() as connection:
        await connection.run_sync(Base.metadata.create_all)
    yield
    await db_engine.dispose()


@pytest_asyncio.fixture(scope='session')
async def session(new_db_schema, db_engine):
    pg_session = async_sessionmaker(db_engine, expire_on_commit=False)
    session = pg_session()
    yield session
    await session.close()
