import os
from argparse import Namespace
from pathlib import Path
from types import SimpleNamespace
from typing import Optional, Union

import asyncpg
import pytest_asyncio
from alembic.autogenerate import compare_metadata
from alembic.config import Config as AlembicConfig

import pytest
from alembic.runtime.environment import EnvironmentContext
from alembic.runtime.migration import MigrationContext
from alembic.script import Script, ScriptDirectory
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine

from src.app.data_sources.models import Base


PROJECT_PATH = Path(__file__).parent.parent.parent.resolve()


def make_alembic_config(cmd_opts: Union[Namespace, SimpleNamespace],
                        base_path: str = PROJECT_PATH) -> AlembicConfig:
    # Replace path to alembic.ini file to absolute
    if not os.path.isabs(cmd_opts.config):
        cmd_opts.config = os.path.join(base_path, cmd_opts.config)

    config = AlembicConfig(file_=cmd_opts.config, ini_section=cmd_opts.name,
                           cmd_opts=cmd_opts)

    # Replace path to alembic folder to absolute
    alembic_location = config.get_main_option('script_location')
    if not os.path.isabs(alembic_location):
        config.set_main_option('script_location',
                               os.path.join(base_path, alembic_location))
    if cmd_opts.pg_url:
        config.set_main_option('sqlalchemy.url', cmd_opts.pg_url)

    return config


def alembic_config_from_url(pg_url: Optional[str] = None) -> AlembicConfig:
    """
    Provides Python object, representing alembic.ini file.
    """
    cmd_options = SimpleNamespace(
        config='alembic.ini', name='alembic', pg_url=pg_url,
        raiseerr=False, x=None,
    )

    return make_alembic_config(cmd_options)


def get_revisions():
    # Create Alembic configuration object
    # (we don't need database for getting revisions list)
    config = alembic_config_from_url()

    # Get directory object with Alembic migrations
    revisions_dir = ScriptDirectory.from_config(config)

    # Get & sort migrations, from first to last
    revisions = list(revisions_dir.walk_revisions('base', 'heads'))
    revisions.reverse()
    return revisions


def do_run_migrations(connection, context):
    context.configure(
        connection=connection, target_metadata=Base.metadata
    )
    with context.begin_transaction():
        context.run_migrations()


async def apply_revision(config: AlembicConfig, revision: str, engine: AsyncEngine, fn: callable) -> None:
    async with engine.begin() as connection:
        with EnvironmentContext(
            config=config,
            script=ScriptDirectory.from_config(config),
            destination_rev=revision,
            fn=fn,
        ) as ctx:
            await connection.run_sync(do_run_migrations, ctx)


@pytest_asyncio.fixture(scope='session')
async def flush_migrations_db(config):
    postgres_table_uri = f'postgresql://{config.postgres.url}/postgres'

    connection = await asyncpg.connect(postgres_table_uri)
    await connection.execute('DROP DATABASE IF EXISTS migrations_test')
    await connection.execute('CREATE DATABASE migrations_test')
    await connection.close()


@pytest_asyncio.fixture(scope='session')
async def alembic_config(flush_migrations_db, config):
    pg_url = f'postgresql+asyncpg://{config.postgres.url}/migrations_test'
    yield alembic_config_from_url(pg_url)


@pytest_asyncio.fixture(scope='function')
async def clear_migrations(alembic_config: AlembicConfig):
    yield
    engine = create_async_engine(alembic_config.cmd_opts.pg_url, echo=True)
    script = ScriptDirectory.from_config(alembic_config)

    def downgrade(rev, context):
        return script._downgrade_revs('base', rev)

    await apply_revision(alembic_config, 'base', engine, downgrade)


@pytest.mark.asyncio
@pytest.mark.parametrize('revision', get_revisions())
async def test_migrations_stairway(alembic_config: AlembicConfig, revision: Script, clear_migrations):
    engine = create_async_engine(alembic_config.cmd_opts.pg_url, echo=True)
    script = ScriptDirectory.from_config(alembic_config)

    def upgrade(rev, context):
        return script._upgrade_revs(revision.revision, rev)

    def downgrade(rev, context):
        return script._downgrade_revs('-1', rev)

    await apply_revision(alembic_config, revision.revision, engine, upgrade)
    # We need -1 for downgrading first migration (its down_revision is None)
    await apply_revision(alembic_config, '-1', engine, downgrade)
    await apply_revision(alembic_config, revision.revision, engine, upgrade)



def get_metadata_diff(connection):
    ctx = MigrationContext.configure(connection)
    return compare_metadata(ctx, Base.metadata)


@pytest.mark.asyncio
async def test_migrations_up_to_date(alembic_config):
    engine = create_async_engine(alembic_config.cmd_opts.pg_url, echo=True)
    script = ScriptDirectory.from_config(alembic_config)

    def upgrade_head(rev, context):
        return script._upgrade_revs('head', rev)
    await apply_revision(alembic_config, 'head', engine, upgrade_head)
    async with engine.connect() as connection:
        diff = await connection.run_sync(get_metadata_diff)
        assert not diff
