from datetime import datetime, timedelta
from decimal import Decimal

import pytest
import pytest_asyncio
from sqlalchemy import insert, text, select, delete

from src.app.config.config import settings
from src.app.data_sources.models import CardAlchemyModel, BalanceLogAlchemyModel, CommonLogAlchemyModel
from src.app.data_sources.transactions import Transactions


@pytest_asyncio.fixture
async def transactions(session):
    yield Transactions(session=session)


def test_check_amount(transactions):
    transactions._check_amount(Decimal('0.01'))


@pytest.mark.parametrize('amount', [
    pytest.param(Decimal('0'), id='Zero amount'),
    pytest.param(Decimal('-0.01'), id='Negative amount'),
])
def test_check_amount_error(amount, transactions):
    with pytest.raises(ValueError):
        transactions._check_amount(amount)


@pytest.mark.asyncio
async def test_change_balance(transactions, card_fixture, session, insert_card):
    new_balance = card_fixture.balance + Decimal('100')
    await transactions._change_balance(
        card=card_fixture,
        amount=new_balance,
    )
    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()
    assert card_from_db.balance == new_balance

    await session.execute(
        delete(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == card_fixture.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_change_balance_error(transactions, card_fixture):
    amount = -settings.card.limit - 1
    with pytest.raises(ValueError):
        await transactions._change_balance(
            card=card_fixture,
            amount=amount,
        )


@pytest.mark.asyncio
async def test_get_balance(transactions, card_fixture, insert_card):
    balance = await transactions.get_balance(
        card_number=card_fixture.card_number,
    )
    assert balance == card_fixture.balance


@pytest.mark.asyncio
async def test_withdrawal(transactions, card_fixture, session, insert_card):
    await transactions.withdrawal(
        card_number=card_fixture.card_number,
        amount=card_fixture.balance,
    )
    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.balance == Decimal('0')

    await session.execute(
        delete(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == card_fixture.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_deposit(transactions, card_fixture, session, insert_card):
    await transactions.deposit(
        card_number=card_fixture.card_number,
        amount=Decimal('123'),
    )
    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.balance == card_fixture.balance + Decimal('123')

    await session.execute(
        delete(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == card_fixture.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_change_limit(transactions, card_fixture, session, insert_card):
    await transactions.change_limit(
        card_number=card_fixture.card_number,
        new_limit=settings.card.verified_limit,
    )
    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.card_limit == settings.card.verified_limit

    await session.execute(
        delete(CommonLogAlchemyModel).where(
            CommonLogAlchemyModel.card_number == card_fixture.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_change_limit_error(transactions, card_fixture):
    with pytest.raises(ValueError):
        await transactions.change_limit(
            card_number=card_fixture.card_number,
            new_limit=Decimal('0'),
        )