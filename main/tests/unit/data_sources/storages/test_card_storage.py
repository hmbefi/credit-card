from decimal import Decimal

import pytest
import pytest_asyncio
from sqlalchemy import insert, text, select, delete
from sqlalchemy.exc import DBAPIError

from src.app.data_sources.models import CardAlchemyModel
from src.app.data_sources.storages.card_storage import CardStorage
from src.app.config.config import settings
from src.app.data_sources.dtos.card import Card


@pytest_asyncio.fixture
async def storage(session):
    yield CardStorage(session=session)


@pytest.mark.asyncio
async def test_add_card(storage, session, card_fixture):
    
    await storage.add(card_fixture.card_number)
    await session.commit()

    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.card_number == card_fixture.card_number
    assert card_from_db.balance == settings.card.default_balance
    assert card_from_db.card_limit == settings.card.limit
    assert card_from_db.is_active

    with pytest.raises(ValueError):
        await storage.add(card_fixture.card_number)

    await session.execute(
        delete(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )


@pytest.mark.asyncio
async def test_get_card(storage, card_fixture, insert_card):
    card_from_db = await storage.get_card(card_fixture.card_number)

    assert card_from_db.card_number == card_fixture.card_number
    assert card_from_db.balance == card_fixture.balance
    assert card_from_db.limit == card_fixture.limit
    assert card_from_db.is_active == card_fixture.is_active


@pytest.mark.asyncio
async def test_get_card_error(storage, card_fixture):
    with pytest.raises(ValueError):
        await storage.get_card(card_fixture.card_number)

@pytest.mark.asyncio
async def test_update_card(storage, session, card_fixture, insert_card):
    updated_card = Card(
        _balance=Decimal('321'),
        card_number=card_fixture.card_number,
        limit=Decimal('321'),
        user_info=None,
        is_active=False,
    )
    await storage.update_card(updated_card)
    await session.commit()

    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.card_number == updated_card.card_number
    assert card_from_db.balance == updated_card.balance
    assert card_from_db.card_limit == updated_card.limit
    assert card_from_db.is_active == updated_card.is_active


@pytest.mark.asyncio
async def test_update_card_error(storage, card_fixture):
    with pytest.raises(ValueError):
        await storage.update_card(card_fixture)


@pytest.mark.asyncio
async def test_close(storage, session, card_fixture, insert_card):
    await storage.close(card_fixture.card_number)
    await session.commit()

    card_from_db = (await session.execute(
        select(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )).scalar()

    assert card_from_db.is_active == False
    


@pytest.mark.asyncio
async def test_close_error(storage, card_fixture):
    with pytest.raises(ValueError):
        await storage.close(card_fixture.card_number)