from datetime import datetime, timedelta
from decimal import Decimal

import pytest
import pytest_asyncio
from src.app.data_sources.models import BalanceLogAlchemyModel, CommonLogAlchemyModel
from sqlalchemy import insert, text, select, delete
from src.app.data_sources.storages.log_storage import LogStorage
from src.app.data_sources.dtos.common_log import CommonLog
from src.app.data_sources.dtos.balance_log import BalanceLog


@pytest_asyncio.fixture
async def storage(session):
    yield LogStorage(session=session)


@pytest.mark.asyncio
async def test_save_common_log(storage, session):
    log = CommonLog(
        card_number='test',
        before='123',
        after='321',
        changes='321',
        datetime_utc=datetime.utcnow(),
    )
    await storage.save_common_log(log)
    await session.commit()

    log_db = (await session.execute(
        select(CommonLogAlchemyModel).where(
            CommonLogAlchemyModel.card_number == 'test',
        )
    )).scalar()

    assert log_db.card_number == log.card_number
    assert log_db.before == log.before
    assert log_db.after == log.after

    await session.execute(
        delete(CommonLogAlchemyModel).where(
            CommonLogAlchemyModel.card_number == log.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_save_balance_log(storage, session):
    log = BalanceLog(
        card_number='test',
        before=Decimal('123'),
        after=Decimal('321'),
        changes=Decimal('321'),
        datetime_utc=datetime.utcnow(),
    )
    await storage.save_balance_log(log)
    await session.commit()

    log_db = (await session.execute(
        select(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == 'test',
        )
    )).scalar()

    assert log_db.card_number == log.card_number
    assert log_db.before == log.before
    assert log_db.after == log.after

    await session.execute(
        delete(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == log.card_number,
        )
    )
    await session.commit()


@pytest.mark.asyncio
async def test_get_balance_history(storage, session):
    log = BalanceLog(
        card_number='test',
        before=Decimal('123'),
        after=Decimal('321'),
        changes=Decimal('321'),
        datetime_utc=datetime.utcnow(),
    )
    for _ in range(5):
        await storage.save_balance_log(log)
    await session.commit()
    
    balance_logs = await storage.get_balance_history(
        card_number='test',
        from_datetime=datetime.utcnow() - timedelta(hours=1),
        to_datetime=datetime.utcnow() + timedelta(hours=1),
    )

    assert len(balance_logs) == 5
    assert balance_logs[0].card_number == log.card_number
    assert balance_logs[0].before == log.before
    assert balance_logs[0].after == log.after

    await session.execute(
        delete(BalanceLogAlchemyModel).where(
            BalanceLogAlchemyModel.card_number == log.card_number,
        )
    )
    await session.commit()
    


@pytest.mark.asyncio
async def test_get_balance_history_error(storage):
    with pytest.raises(ValueError):
        logs = await storage.get_balance_history(
            card_number='test',
            from_datetime=datetime.utcnow() + timedelta(hours=1),
            to_datetime=datetime.utcnow() - timedelta(hours=1),
        )
