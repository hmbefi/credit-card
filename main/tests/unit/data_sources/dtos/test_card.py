from decimal import Decimal

import pytest


@pytest.mark.parametrize('balance', [
    pytest.param(Decimal('123'), id='Positive balance'),
    pytest.param(Decimal('5000'), id='Positive balance equal to limit'),
    pytest.param(Decimal('6000'), id='Positive balance greater than limit'),
    pytest.param(Decimal('-123'), id='Negative balance'),
    pytest.param(Decimal('-5000'), id='Negative balance equal to negative limit'),
])
def test_balance_setter(balance, card_fixture):
    card_fixture.balance = balance
    assert card_fixture._balance == balance

def test_balance_setter_error(card_fixture):
    with pytest.raises(ValueError):
        card_fixture.balance = Decimal('-5000.01')