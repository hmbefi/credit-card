from datetime import datetime, timedelta
from decimal import Decimal

import pytest
import pytest_asyncio

from sqlalchemy import insert, delete

from src.app.data_sources.dtos.card import Card
from src.app.data_sources.models import CardAlchemyModel, BalanceLogAlchemyModel, CommonLogAlchemyModel



@pytest.fixture
def card_fixture():
    card = Card(
        _balance=Decimal('123'),
        card_number='test',
        limit=Decimal('5000'),
        user_info={},
        is_active=True,
    )
    return card


@pytest_asyncio.fixture
async def insert_card(session, card_fixture):
    await session.execute(
        insert(CardAlchemyModel).values(
            card_number=card_fixture.card_number,
            balance=card_fixture.balance,
            card_limit=card_fixture.limit,
            is_active=card_fixture.is_active,
        )
    )
    await session.commit()
    yield
    await session.execute(
        delete(CardAlchemyModel).where(
            CardAlchemyModel.card_number == card_fixture.card_number,
        )
    )
    await session.commit()

