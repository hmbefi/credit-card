"""Модуль содержащий эндпоинты связанные с кредитными картами."""

from datetime import datetime
from decimal import Decimal

from fastapi import APIRouter, HTTPException, responses, status

from src.app.api import schemas

router = APIRouter()
transactions = None


@router.get('/api/ready')
async def ready():
    """Ready проба.

    Returns:
        Response: статус сервиса
    """
    is_db_live = await transactions.check_db()
    if is_db_live:
        return responses.Response(status_code=status.HTTP_200_OK)
    return responses.Response(status_code=status.HTTP_503_SERVICE_UNAVAILABLE)


@router.get('/api/live')
async def live():
    """Liveness проба.

    Returns:
        Response: статус сервиса
    """
    return responses.Response(status_code=status.HTTP_200_OK)


@router.post('/api/add')
async def add_card(card_number: str):
    """Добавление новой карты.

    Args:
        card_number (str): номер карты

    Raises:
        HTTPException: карта с таким номером уже существует
    """
    try:
        await transactions.add_card(card_number=card_number)
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )


@router.get('/api/balance')
async def get_balance(card_number: str) -> schemas.BalanceResponse:
    """Получение баланса карты.

    Args:
        card_number (str): номер карты

    Raises:
        HTTPException: карта с таким номером не найдена

    Returns:
        BalanceResponse: баланс карты
    """
    try:
        balance = await transactions.get_balance(card_number=card_number)
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )
    return schemas.BalanceResponse(card_number=card_number, balance=str(balance))


@router.get('/api/balance/history')
async def get_balance_history(
    card_number: str,
    from_datetime: datetime,
    to_datetime: datetime,
) -> list[schemas.BalanceLogResponse]:
    """Получение истории изменений баланса по карте.

    Args:
        card_number (str): номер карты
        from_datetime (datetime):
        дата и время с которых начинается поиск записей
        to_datetime (datetime):
        дата и время на которых заканчивается поиск записей

    Raises:
        HTTPException: from_datetime должно быть раньше to_datetime

    Returns:
        list[dict]: список записей за указанный промежуток времени
    """
    try:
        balance_history = await transactions._history.get_balance_history(  # noqa: WPS437
            card_number=card_number,
            from_datetime=from_datetime,
            to_datetime=to_datetime,
        )
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )
    return balance_history


@router.get('/api/withdrawal')
async def withdrawal(card_number: str, amount: Decimal):
    """Метод вывода средств с карты.

    Args:
        card_number (str): номер карты
        amount (Decimal): значение для вывода

    Raises:
        HTTPException: ошибка при попытке вывода с карты
    """
    try:
        await transactions.withdrawal(
            card_number=card_number,
            amount=amount,
        )
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )


@router.get('/api/deposit')
async def deposit(card_number: str, amount: Decimal):
    """Метод перевода средств на карту.

    Args:
        card_number (str): номер карты
        amount (Decimal): значение для перевода

    Raises:
        HTTPException: ошибка при попытке перевода на карту
    """
    try:
        await transactions.deposit(
            card_number=card_number,
            amount=amount,
        )
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )


@router.get('/api/limit')
async def get_limit(card_number: str) -> schemas.LimitResponse:
    """Получение кредитного лимита карты.

    Args:
        card_number (str): номер карты

    Raises:
        HTTPException: карта с таким номером не найдена

    Returns:
        LimitResponse: лимит карты
    """
    try:
        card = await transactions._card_storage.get_card(card_number=card_number)  # noqa: WPS437
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )
    return schemas.LimitResponse(card_number=card_number, limit=str(card.limit))


@router.get('/api/limit/change')
async def change_limit(card_number: str, limit: Decimal):
    """Изменения лимита кредитной карты.

    Args:
        card_number (str): номер карты
        limit (Decimal): новый лимит

    Raises:
        HTTPException: ошибка при изменении лимита
    """
    try:
        await transactions.change_limit(
            card_number=card_number,
            new_limit=limit,
        )
    except ValueError as exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(exception),
        )
