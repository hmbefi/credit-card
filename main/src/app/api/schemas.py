"""Модуль содержит модели (схемы) для валидации данных."""

from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel


class BalanceResponse(BaseModel):
    """Класс ответа на запрос баланса."""

    card_number: str
    balance: Decimal


class BalanceLogResponse(BaseModel):
    """Класс ответа на запрос баланса."""

    card_number: str
    before: Decimal
    after: Decimal
    changes: Decimal
    datetime_utc: datetime


class LimitResponse(BaseModel):
    """Класс ответа на запрос лимита."""

    card_number: str
    limit: Decimal
