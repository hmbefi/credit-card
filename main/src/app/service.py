"""Модуль является точкой входа в приложение."""

from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI

from src.app.config.config import settings
from src.app.credit_cards import controller as credit_cards_controller
from src.app.data_sources.adaptor import DatabaseConnection
from src.app.data_sources.transactions import Transactions


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Метод производит start и shutdown ивенты.

    Args:
        app (FastAPI): приложение

    Yields:
        None
    """
    connection = DatabaseConnection()
    credit_cards_controller.transactions = Transactions(connection.get_session())
    yield


app = FastAPI(lifespan=lifespan)
app.include_router(credit_cards_controller.router)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=settings.service.host,
        port=settings.service.port,
    )
