"""Модуль содержит класс BalanceLog."""

from dataclasses import dataclass
from decimal import Decimal

from src.app.data_sources.dtos.common_log import CommonLog
from src.app.data_sources.models.balance_log import BalanceLogAlchemyModel


@dataclass
class BalanceLog(CommonLog):
    """Класс наследуемый от CommonLog, содержит изменения связанные с балансом.

    Args:
        before (Decimal): значение до изменения
        after (Decimal): значение после изменения
        changes (Decimal): величина изменения
    """

    before: Decimal
    after: Decimal
    changes: Decimal

    @classmethod
    def from_alchemy(cls, record: BalanceLogAlchemyModel):
        """Метод для инициализации на данных из бд.

        Args:
            record (BalanceLogAlchemyModel): запись лога из бд

        Returns:
            BalanceLog: лог изменения баланса
        """
        return cls(
            card_number=record.card_number,
            before=record.before,
            after=record.after,
            changes=record.after,
            datetime_utc=record.datetime_utc,
        )
