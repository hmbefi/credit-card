"""Модуль содержит класс User."""

from dataclasses import dataclass
from decimal import Decimal

from src.app.data_sources.models.card import CardAlchemyModel


@dataclass
class Card(object):
    """Dataclass содержащий информацию о кредитной карте.

    Args:
        _balance (Decimal): текущий баланс карты
        card_number (str): номер карты
        limit (Decimal): текущий кредитный лимит карты
        user_info (dict): дополнительная информация о владельце карты
        is_acitve (bool): открыта или закрыта карта
    """

    _balance: Decimal
    card_number: str
    limit: Decimal
    user_info: dict
    is_active: bool

    @property
    def balance(self):
        """Метод getter использующий декоратор property.

        Returns:
            Decimal: текущий баланс карты
        """
        return self._balance

    @balance.setter
    def balance(self, balance: Decimal):
        if balance < -abs(self.limit):
            raise ValueError('Баланс не может быть меньше отрицательного кредитного лимита')
        self._balance = balance

    @classmethod
    def from_alchemy(cls, record: CardAlchemyModel, user_info: dict = None):
        """Метод для инициализации на данных из бд.

        Args:
            record (CardAlchemyModel): запись кредитной карты из бд
            user_info (dict): дополнительная информация о владельце карты

        Returns:
            Card: объект класса Card
        """
        return cls(
            _balance=record.balance,
            card_number=record.card_number,
            limit=record.card_limit,
            user_info=user_info,
            is_active=record.is_active,
        )
