"""Модуль содержит класс-интерфейс CommonLog."""

from dataclasses import dataclass
from datetime import datetime
from typing import Any

from src.app.data_sources.models.common_log import CommonLogAlchemyModel


@dataclass
class CommonLog(object):
    """Класс-интерфейс для любых логов в приложении.

    Args:
        card_number (str): номер карты
        before: значение до изменения
        after: значение после изменения
        changes: информация о том что изменяется или величина изменения
        datetime_utc (datetime): дата и время изменения
    """

    card_number: str
    before: Any
    after: Any
    changes: Any
    datetime_utc: datetime

    @classmethod
    def from_alchemy(cls, record: CommonLogAlchemyModel):
        """Метод для инициализации на данных из бд.

        Args:
            record (CommonLogAlchemyModel): запись лога из бд

        Returns:
            CommonLog: объект класса CommonLog
        """
        return cls(
            card_number=record.card_number,
            before=record.before,
            after=record.after,
            changes=record.after,
            datetime_utc=record.datetime_utc,
        )
