"""Модуль содержит класс для создания сессий подключения к бд."""
from asyncio import current_task

from sqlalchemy.ext import asyncio as sa_asyncio

from src.app.config.config import settings


class DatabaseConnection(object):
    """Класс для создания сессий подключения к бд."""

    def __init__(self):
        self._engine = sa_asyncio.create_async_engine(
            url=settings.postgres.uri,
        )
        async_session_factory = sa_asyncio.async_sessionmaker(
            self._engine,
            expire_on_commit=False,
        )
        self.async_scoped_session = sa_asyncio.async_scoped_session(
            async_session_factory,
            scopefunc=current_task,
        )

    def get_session(self) -> sa_asyncio.AsyncSession:
        """Метод для создания новой сессии подключения к бд.

        Returns:
            AsyncSession: сессия подключения к бд
        """
        return self.async_scoped_session()
