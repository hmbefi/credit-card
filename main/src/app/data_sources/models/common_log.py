"""Модуль содержит orm модель common log."""
from sqlalchemy import BigInteger, Column, DateTime, String, text

from src.app.data_sources.models.base import Base


class CommonLogAlchemyModel(Base):
    """Класс описывает orm модель common log.

    Args:
        Base (DeclarativeMeta): базовая orm модель
    """

    __tablename__ = 'common_logs'

    id = Column(BigInteger, primary_key=True)
    card_number = Column(String(length=16), index=True, nullable=False)  # noqa: WPS432
    before = Column(String(length=100), nullable=False)  # noqa: WPS432
    after = Column(String(length=100), nullable=False)  # noqa: WPS432
    datetime_utc = Column(DateTime, server_default=text("TIMEZONE('utc', CURRENT_TIMESTAMP)"))
