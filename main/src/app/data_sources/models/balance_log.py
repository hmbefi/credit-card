"""Модуль содержит orm модель balance log."""
from sqlalchemy import BigInteger, Column, DateTime, Numeric, String, text

from src.app.data_sources.models.base import Base


class BalanceLogAlchemyModel(Base):
    """Класс описывает orm модель balance log.

    Args:
        Base (DeclarativeMeta): базовая orm модель
    """

    __tablename__ = 'balance_logs'

    id = Column(BigInteger, primary_key=True)
    card_number = Column(String(length=16), index=True, nullable=False)  # noqa: WPS432
    before = Column(Numeric(precision=50, scale=2), nullable=False)  # noqa: WPS432
    after = Column(Numeric(precision=50, scale=2), nullable=False)  # noqa: WPS432
    datetime_utc = Column(DateTime, server_default=text("TIMEZONE('utc', CURRENT_TIMESTAMP)"))
