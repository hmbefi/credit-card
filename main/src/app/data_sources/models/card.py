"""Модуль содержит orm модель кредитной карты."""
from sqlalchemy import BigInteger, Boolean, Column, Numeric, String

from src.app.data_sources.models.base import Base


class CardAlchemyModel(Base):
    """Класс описывает orm модель кредитной карты.

    Args:
        Base (DeclarativeMeta): базовая orm модель
    """

    __tablename__ = 'cards'

    id = Column(BigInteger, primary_key=True)
    balance = Column(Numeric(precision=50, scale=2), nullable=False)  # noqa: WPS432
    card_number = Column(String(length=16), index=True, nullable=False, unique=True)  # noqa: WPS432
    card_limit = Column(Numeric(precision=50, scale=2), nullable=False)  # noqa: WPS432
    is_active = Column(Boolean, nullable=False)
