"""Модуль содержит класс Transactions."""

from decimal import Decimal

from sqlalchemy import select
from sqlalchemy.exc import DBAPIError
from sqlalchemy.ext.asyncio import AsyncSession

from src.app.data_sources.dtos.balance_log import BalanceLog
from src.app.data_sources.dtos.card import Card
from src.app.data_sources.dtos.common_log import CommonLog
from src.app.data_sources.models import CardAlchemyModel
from src.app.data_sources.storages.card_storage import CardStorage
from src.app.data_sources.storages.log_storage import LogStorage


class Transactions(object):  # noqa: WPS214
    """Класс транзакций для получения, добавления и изменения данных кредитных карт."""

    def __init__(self, session: AsyncSession):
        self._session = session
        self._card_storage = CardStorage(session)
        self._history = LogStorage(session)

    async def check_db(self):
        """Метод для проверки базы данных.

        Returns:
            bool: жива ли бд
        """
        try:
            await self._session.execute(
                select(CardAlchemyModel).where(
                    CardAlchemyModel.id == 0,
                ),
            )
        except DBAPIError:
            return False
        return True

    async def get_balance(self, card_number: str) -> Decimal:
        """Метод возвращает текущий баланс пользователя.

        Args:
            card_number (str): номер карты

        Returns:
            Decimal: текущий баланс
        """
        card = await self._card_storage.get_card(card_number)
        return card.balance

    async def withdrawal(self, card_number: str, amount: Decimal):
        """Метод вывода средств с карты.

        Args:
            card_number (str): номер карты
            amount (Decimal): значение для вывода
        """
        self._check_amount(amount)
        card = await self._card_storage.get_card(card_number)
        await self._change_balance(card, card.balance - amount)
        await self._session.commit()

    async def deposit(self, card_number: str, amount: Decimal):
        """Метод перевода средств на карту.

        Args:
            card_number (str): номер карты
            amount (Decimal): значение для перевода
        """
        self._check_amount(amount)
        card = await self._card_storage.get_card(card_number)
        await self._change_balance(card, card.balance + amount)
        await self._session.commit()

    async def change_limit(self, card_number: str, new_limit: Decimal):
        """Метод для изменения кредитного лимита карты.

        Args:
            card_number (str): номер карты
            new_limit (Decimal): новый лимит

        Raises:
            ValueError: кредитный лимит должен быть больше нуля
        """
        if new_limit < 0:
            raise ValueError('Кредитный лимит должен быть больше нуля')

        card = await self._card_storage.get_card(card_number)
        log = CommonLog(
            card_number=card_number,
            before=card.limit,
            after=new_limit,
            changes=new_limit,
            datetime_utc=None,
        )
        card.limit = new_limit
        await self._card_storage.update_card(card)
        await self._history.save_common_log(log)
        await self._session.commit()

    async def add_card(self, card_number: str):
        """Метод добавления новых кредитных карт.

        Args:
            card_number (str): номер карты
        """
        log = CommonLog(
            card_number=card_number,
            before='not_created',
            after=card_number,
            changes=card_number,
            datetime_utc=None,
        )
        await self._card_storage.add(card_number=card_number)
        await self._history.save_common_log(log)
        await self._session.commit()

    async def _change_balance(self, card: Card, amount: Decimal):
        """Метод для изменения баланса карты.

        Args:
            card (Card): карта
            amount (Decimal): значение нового баланса
        """
        balance_log = BalanceLog(
            card_number=card.card_number,
            before=card.balance,
            after=amount,
            changes=amount,
            datetime_utc=None,
        )
        card.balance = amount
        await self._card_storage.update_card(card)
        await self._history.save_balance_log(balance_log)
        await self._session.commit()

    def _check_amount(self, amount: Decimal):
        """Метод для проверки значения amount.

        Args:
            amount (Decimal): значение (сумма)

        Raises:
            ValueError: значение должно быть больше нуля
        """
        if amount <= 0:
            raise ValueError('Значение должно быть больше нуля')
