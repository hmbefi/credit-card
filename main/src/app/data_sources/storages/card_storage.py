"""Модуль содержит класс CardStorage."""

from sqlalchemy import insert, select, update

from src.app.config.config import settings
from src.app.data_sources.dtos.card import Card
from src.app.data_sources.models import CardAlchemyModel
from src.app.data_sources.storages.base import BaseStorage


class CardStorage(BaseStorage):
    """Класс хранилища кредитных карт."""

    async def add(self, card_number: str):
        """Метод добавления новых кредитных карт.

        Args:
            card_number (str): номер карты

        Raises:
            ValueError: карта с таким номером уже существует
        """
        card = (await self._session.execute(
            select(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card_number,
            ),
        )).scalar()

        if card:
            raise ValueError('Карта с таким номером уже существует')

        await self._session.execute(
            insert(CardAlchemyModel).values(
                card_number=card_number,
                balance=settings.card.default_balance,
                card_limit=settings.card.limit,
                is_active=True,
            ),
        )

    async def get_card(self, card_number: str) -> Card:
        """Метод для получения карты.

        Args:
            card_number (str): номер карты

        Raises:
            ValueError: карта с таким номером не найдена

        Returns:
            Card: карта
        """
        card = (await self._session.execute(
            select(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card_number,
            ),
        )).scalar()

        if not card:
            raise ValueError('Карта с таким номером не найдена')
        return Card.from_alchemy(card)

    async def update_card(self, card: Card):
        """Метод для перезаписи карты.

        Args:
            card (Card): обновленная карта карта

        Raises:
            ValueError: карта с таким номером не найдена
        """
        card_exists = (await self._session.execute(
            select(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card.card_number,
            ),
        )).scalar()
        if not card_exists:
            raise ValueError('Карта с таким номером не найдена')

        await self._session.execute(
            update(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card.card_number,
            ).values(
                balance=card.balance,
                card_limit=card.limit,
                is_active=card.is_active,
            ),
        )

    async def close(self, card_number: str):
        """Метод для закрытия карты.

        Args:
            card_number (str): номер карты

        Raises:
            ValueError: карта с таким номером не найдена
        """
        card = (await self._session.execute(
            select(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card_number,
            ),
        )).scalar()
        if not card:
            raise ValueError('Карта с таким номером не найдена')

        await self._session.execute(
            update(CardAlchemyModel).where(
                CardAlchemyModel.card_number == card_number,
            ).values(
                is_active=False,
            ),
        )
