"""Модуль содержит класс LogStorage."""

from datetime import datetime

from sqlalchemy import insert, select

from src.app.data_sources import models
from src.app.data_sources.dtos.balance_log import BalanceLog
from src.app.data_sources.dtos.common_log import CommonLog
from src.app.data_sources.storages.base import BaseStorage


class LogStorage(BaseStorage):
    """Класс хранилища всех логов приложения."""

    async def save_common_log(
        self,
        common_log: CommonLog,
    ):
        """Метод для сохранения лога изменения.

        Args:
            common_log (CommonLog): лог изменения
        """
        await self._session.execute(
            insert(models.CommonLogAlchemyModel).values(
                card_number=common_log.card_number,
                before=str(common_log.before),
                after=str(common_log.after),
            ),
        )

    async def save_balance_log(
        self,
        balance_log: BalanceLog,
    ):
        """Метод для сохранения лога изменения баланса.

        Args:
            balance_log (BalanceLog): лог изменения баланса
        """
        await self._session.execute(
            insert(models.BalanceLogAlchemyModel).values(
                card_number=balance_log.card_number,
                before=balance_log.before,
                after=balance_log.after,
            ),
        )

    def extract_balance_history(
        self,
        balance_log_records: list,
        from_datetime: datetime,
        to_datetime: datetime,
    ) -> list[BalanceLog]:
        """Метод для извлечения нужной истории изменения баланса.

        Args:
            balance_log_records (list): записи истории изменения баланса
            from_datetime (datetime):
            дата и время с которых начинается поиск записей
            to_datetime (datetime):
            дата и время на которых заканчивается поиск записей

        Returns:
            list[BalanceLog]: список с логами изменений баланса
        """
        balance_history = []

        for balance_log in balance_log_records[::-1]:
            if to_datetime >= balance_log.datetime_utc:
                if from_datetime <= balance_log.datetime_utc:
                    balance_history.append(BalanceLog.from_alchemy(balance_log))
                else:
                    return balance_history[::-1]

        return balance_history[::-1]

    async def get_balance_history(
        self,
        card_number: str,
        from_datetime: datetime,
        to_datetime: datetime,
    ) -> list[BalanceLog]:
        """Метод для получения истории изменения баланса.

        Args:
            card_number (str): номер карты
            from_datetime (datetime):
            дата и время с которых начинается поиск записей
            to_datetime (datetime):
            дата и время на которых заканчивается поиск записей

        Raises:
            ValueError: from_datetime должна быть раньше to_datetime

        Returns:
            list[BalanceLog]: список с логами изменений баланса
        """
        if from_datetime > to_datetime:
            raise ValueError('Указаны неверные границы промежутка времени')

        balance_logs = (await self._session.execute(
            select(models.BalanceLogAlchemyModel).where(
                models.BalanceLogAlchemyModel.card_number == card_number,
            ),
        )).scalars().all()

        return self.extract_balance_history(
            balance_log_records=balance_logs,
            from_datetime=from_datetime,
            to_datetime=to_datetime,
        )
