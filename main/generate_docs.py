import json

from src.app.service import app


app_docs = app.openapi()

with open('src/app/api/openapi.json', 'w', encoding='utf8') as f:
    json.dump(app_docs, f, ensure_ascii=False)
