-- Active: 1696164279631@@127.0.0.1@5432@credit_card
CREATE TABLE users (
  id bigserial PRIMARY KEY,
  username varchar(100) NOT NULL,
  password_hash varchar(60) NOT NULL
);

INSERT INTO users (username, password_hash) VALUES ('user', '$2b$12$q0YoQYC2JaizbXzXxH2b/.WXVTSDm2aHjZCV0thvKYQm8BwbcC44C');

INSERT INTO users (username, password_hash) VALUES ('admin', '$2b$12$chpeWokxtSIvrADVq90fYuxCTXpX...CCnw0AUNWyq9YnQsCPf5lu');

SELECT * FROM users;

SELECT * FROM users WHERE username = 'user';

UPDATE users SET password_hash = '$2b$12$fZGbwESWPS4eutYEZjWomOYlCgOnpifmZ5BOC4lwD4bUj6Lz6GHZi' where username = 'user';

DELETE FROM users where username = 'user'
