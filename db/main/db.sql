CREATE TABLE cards (
  id bigserial PRIMARY KEY,
  balance decimal(50, 2) NOT NULL,
  card_number varchar(16) NOT NULL,
  card_limit decimal(50, 2) NOT NULL
);

INSERT INTO cards (balance, card_number, card_limit) VALUES ('0', 'test', '10000');

INSERT INTO cards (balance, card_number, card_limit) VALUES ('0', '1234123412341234', '5000');

SELECT * FROM cards;

SELECT balance FROM cards WHERE card_number = 'test';

UPDATE cards SET balance = '123.5' where card_number = 'test';

DELETE FROM cards where card_number = '1234123412341234'

CREATE TABLE balance_logs (
  id bigserial PRIMARY KEY,
  card_number varchar(16),
  before decimal(50, 2),
  after decimal(50, 2),
  datetime_utc timestamp
);

INSERT INTO balance_logs (card_number, before, after, datetime_utc) VALUES ('test', '0', '123.5', '2023-09-30 21:00:01.123');

INSERT INTO balance_logs (card_number, before, after, datetime_utc) VALUES ('test', '123.5', '100', '2023-09-30 22:22:22');

INSERT INTO balance_logs (card_number, before, after, datetime_utc) VALUES ('1234123412341234', '0', '1000', '2023-09-25 12:30:00');

SELECT * FROM balance_logs;

SELECT * FROM balance_logs WHERE card_number = 'test' AND datetime_utc <= '2023-09-30 22:00:00';

UPDATE balance_logs SET before = '-100' where datetime_utc = '2023-09-30 21:00:01.123';

DELETE FROM balance_logs where card_number = '1234123412341234';

SELECT cards.card_number, cards.balance, balance_logs.before, balance_logs.after
FROM cards
INNER JOIN balance_logs
ON cards.card_number = balance_logs.card_number;

DELETE FROM balance_logs where card_number = 'test';

SELECT * FROM cards;

SELECT * FROM balance_logs WHERE card_number = 'test';

BEGIN;
    UPDATE cards SET balance = '-1000' where card_number = 'test';
    INSERT INTO balance_logs (card_number, before, after, datetime_utc) VALUES ('test', '123.5', '-1000', '2023-09-30 22:22:22');
COMMIT;

